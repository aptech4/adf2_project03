import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            //Khởi tạo menu ứng dụng
            Scanner scanner = new Scanner(System.in);

            System.out.print("1. Tra cứu từ điển.\n2. Bổ sung từ điển.\n3. Thoát.\nNhập lựa chọn của bạn: ");
            int option = 0;
            try {
                option = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Tuỳ chọn bạn vừa nhập không đúng định dạng. Vui lòng nhập lại!");
                continue;
            }

            switch (option) {
                case 1:
                    scanner = new Scanner(System.in);
                    System.out.print("\nNhập từ cần tìm: ");
                    String keyword = scanner.nextLine();

                    //tìm nội dung từ vựng trong database
                    String description = findDescriptionOfKeyword(keyword);
                    //nếu nội dung là null tức là không tìm thấy từ
                    if (description == null) {
                        System.out.println("Không tìm thấy từ vựng này!\n");
                    } else {
                        System.out.printf("Ý nghĩa là: %s\n\n", description);
                    }
                    break;
                case 2:
                    while (true) {
                        scanner = new Scanner(System.in);
                        System.out.print("\nNhập từ vựng: ");
                        String newKeyword = scanner.nextLine();

                        //tái sử dụng hàm tìm ý nghĩa của từ vựng, nếu từ mới vừa nhập vào mà
                        //tìm thấy trong database tức là từ này đã tồn tại, yêu cầu người dùng nhập từ khác
                        if (findDescriptionOfKeyword(newKeyword) != null) {
                            System.out.println("Từ vựng đã tồn tại. Vui lòng nhập từ khác.");
                            continue;
                        }
                        //code chạy đến đây tức là từ mới vừa nhập chưa tồn tại, cho người dùng nhập ý nghĩa của từ và lưu vào DB
                        System.out.print("Nhập ý nghĩa: ");
                        String newDescription = scanner.nextLine();

                        Dictionary dictionary = new Dictionary(newKeyword, newDescription);
                        saveDictionary(dictionary);
                        break;
                    }
                    break;
                case 3:
                    return;
                default:
                    break;
            }
        }
    }

    //function tìm kiếm nội dung giải nghĩa của từ vựng bất kì.
    // nếu tìm thấy từ vựng tương ứng từ cần tìm, trả về giaải nghĩa của từ đó
    // nếu không tìm thấy từ vựng tương ứng, trả về null
    public static String findDescriptionOfKeyword(String searchValue) {
        File file = new File("vdict.txt");
        //check nếu file chưa tồn tại, return null, không xử lý nữa
        if (!file.exists()) {
            return null;
        }

        try {
            //khởi tạo lớp scanner để đọc từng dòng nội dung của file
            Scanner scanner = new Scanner(file);
            //vòng lặp - chừng nào scanner thấy còn dòng dữ liệu để đọc thì tiếp tục đọc ra và xử lý
            while (scanner.hasNextLine()) {
                //Đọc ra dòng dữ liệu
                String lineData = scanner.nextLine();
                //Bóc tách dòng dữ liệu theo cú pháp lưu Từ vựng | Giải nghĩa
                String[] elements = lineData.split("\\|");
                //Nếu số phần tử khác 2, tức là dòng dữ liệu này không hợp lệ, bỏ qua không xử lý, chuyển qua dòng kế tiếp
                if (elements.length != 2) {
                    continue;
                }
                //Lấy ra từ vựng trong dòng dữ liệu, vì lưu cấu trúc Từ vựng | Giải nghĩa, nên phải trim() dấu cách ở cuối từ vựng
                String keyword = elements[0].trim();
                //Lấy ra giải nghĩa
                String description = elements[1].trim();
                //Kiểm tra nếu từ vựng của dòng dữ liệu trong database mà trùng với từ đang cần tìm kiếm
                //thì trả về nội dung giải nghĩa của từ vựng tương ứng đã lưu vào DB và kết thúc chương trình
                if (keyword.equalsIgnoreCase(searchValue)) {
                    scanner.close();
                    return description;
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        //nếu code chạy đến đây là không tìm thấy từ vựng trong DB, return null
        return null;
    }

    public static void saveDictionary(Dictionary dictionary) {
        File file = new File("vdict.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Không thể khởi tạo tệp trong hệ thống này!");
                return;
            }
        }

        try {
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write(dictionary.getKeyword() + " | " + dictionary.getDescription() + "\n");
            fileWriter.close();
            System.out.println("Lưu từ vựng thành công!\n");
        } catch (IOException e) {
            System.out.println("Ghi tệp không thành công!");
        }
    }
}